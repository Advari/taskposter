'use strict'

class CreateUser {
  get rules () {
    return {
        'username' : 'required|unique:users',
        'email' : 'required|unique:users',
        'password' : 'required|unique:users'
    }
  }

  // Defining the messages in order to return the proper message while receiving validation fault
  get messages(){
    return {
      'required': '{{field}} is required',
      'unique': '{{field}} already exists',
    }
  }

// if the validation failed .... 
  async fails(error){
    // ctx is short for context 
    this.ctx.session.withErrors(error)
    .flashAll();

    return this.ctx.response.redirect('back')
  }
}

module.exports = CreateUser
