'use strict'

class CreateTask {
  get rules () {
    return {
      title : 'required',
      link : 'required'
    }
  }

  get messages() {
    return {
      'required': ' {{field}} is required '
    }
  }
}

module.exports = CreateTask
