'use strict'

class UpdateUser {
  get rules () {
    return {
      email : 'required|unique:users,email',
      username : 'required|unique:users|alphanumeric'
    }
  }


  get messages(){
    return {
      'required' : '{{field}} is required',
      'unique' : '{{field}} already used '
    }
  }


  async fails(error){
    this.ctx.session.withErrors(error)
      .flashAll();
  }
}

module.exports = UpdateUser
