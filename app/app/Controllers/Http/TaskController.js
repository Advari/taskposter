'use strict'

const { query } = require("../../Models/Task")

// importing the model 
const Task = use('App/Models/task')
const Helpers = use('Helpers')
const Drive = use('Drive')
const Database = use('Database')


// the controller is here to control deleting,updating,... the task Model. 
class TaskController {
    async home() { 

        // fetch a task 
        const tasks = await Task.all();  // Check Different queries in Adonis Docs
        return tasks.toJSON();
    }

    async userIndex( { auth,request }){


        const query = request.get()
        const search = query.search;
        const sort = query.sort;
        const sort_type = query.sort_type;
        const page = query.page;
        const page_size = query.page_size
        
        console.log(page , page_size , search , sort , sort_type)
        
        let tasks;
        if(search){
            try{
                tasks =  auth.user.tasks()
                            .orderBy(sort,sort_type)
                            .where('title','like' , `%${search}%`)
                            .orWhere('description','like',`%${search}%`)
                            .orWhere('link','like',`%${search}%`)

            } catch {
                 return 'Authentication Failed'
            }
        }
        else 
            tasks = auth.user.tasks().orderBy(sort,sort_type)

        if(page && page_size)
            tasks = await tasks.paginate(page,page_size)
        else
            tasks = await tasks.fetch()

        return tasks.toJSON();
    }

    async create( { request , response , session , auth }){
        const task = request.all();
    
        const posted = await auth.user.tasks().create({
            title:task.title,
            link:task.link,
            description: task.description,
            expire:task.expire
        })

        return 'Task Has been Posted ';
    }


    async delete({ response , session , params}){
        const task= await Task.find(params.id);
        await task.delete();
        return 'Task has been deleted'
    }

    async update({response , request , session , params}){
        const task = await Task.find(params.id);

        task.title = request.all().title;
        task.link = request.all().link;
        task.description = request.all().description;

        await task.save();

        return 'Task Updated'
    }

    async setPic({params,request,auth}){
            let user;
            try{
                user = await auth.getUser();
            }
            catch{
                return 'Authentication failed'; 
            }
            
                const id = params.id; 
                // user.tasks() returns a query ( modeling sql queries are possible )
                // output should be converted to JSON 
                let task = await user.tasks().where('id', id).first();

                if(!task)
                    return 'Not Exists'

                task = task.toJSON();
            
                // the name (here : uploadedFile ) should be provided in form-data (key-value) pairs 
                const receivedFile = await request.file('task-pic' , {
                    "types":['image'],
                    "size" : '2mb'
                });
    
                await receivedFile.move(Helpers.tmpPath(`uploads/${user.id}/tasks/${id}/picture`) , {
                    name:`tp_${id}.${receivedFile.extname}`
                });
    
                if(!receivedFile.moved()){
                    return receivedFile.error();
                }
                return 'Task Pic Saved'; 
    }

    async sendTf({response,params}){
        const id = params.id;
        let ownerId = await Database
                    .from('tasks')
                    .select('user_id')
                    .where('id',id)
                    .first();
        ownerId = ownerId.user_id;
        console.log(ownerId)

        response.header('Content-Type', 'image/jpg');
        await response.attachment(Helpers.tmpPath(`uploads/${ownerId}/tasks/${id}/picture/tp_${id}.jpg`))
    }

}

module.exports = TaskController
