'use strict'
const User = use('App/Models/User')
const Mail = use('Mail');
const Drive = use('Drive')
const Helpers = use('Helpers')


class UserController {
    // parameters are the things we want to get from the context 
    async create( { request , auth }) { 

        // here we are sure that the data is valid. 
        const user = await User.create( request.only(['username', 'email' , 'password'])); 

        // login the created user / generate and send a token 
        const token = await auth.generate(user);
        return token; 
    }

    async login( { request, auth}){
        const {email , password} = request.all();

        try{
            const token = await auth.attempt(email, password);
            return token;
        }
        catch{
            return 'Authentication Failed!'
        }
    }

    
    async delete( { auth }){

        try{
            const user = await  auth.getUser() ; 

            console.log(user)
        
            await user.delete(); 
            return user; 
        }
        catch {
            return 'Authentication Failed'
        }
    }


    async update( { auth  , request }){

        try {
        
            const user = await auth.getUser();
            const {username , email} = request.all(); 
            user.username = username;
            user.email = email; 

            await user.save();
            return user;
        }
        catch {
            return 'Authentication Failed'
        }
    }

    async show({ request }){

        let users; 

        const query = request.get();
        const id = query.id
        const page = query.page 
        const page_size = query.page_size


        if (id){
             const user = await User.find(id);

             if(!user)
                return 'User Not Found'

             return { email:user.email , username:user.username }
        }
        else{
            users = User.query() 
                        .select('username','email')                                         
        }

        if(!users)
            return 'User Not Found'

        if( page && page_size)
            return await users.paginate(page,page_size);
            
        return await users.fetch()
    }



    async forget({request , auth}){
        const {email} = request.all();

        // find the corresponding user
        const user = await User.findBy({email : email});
        if(!user)
            return 'The user with the given email has not registered yet'
        

        const token =  (await auth.generate(user)).token;
        const link = 'http://localhost/resetPass/' + token ; 
        await Mail.raw( `<a href="${link}">Click On This </a>` , (message)=>{
            message.from('noreply@app.com')
            message.to(email)
        }); 
        
    }
    async resetPass({request , auth , params}){
        try {
            const token = params.token;
            request.request.headers.authorization = `Bearer ${token}`;
            
            const user = auth.getUser();
            const {password} = request.all();
            user.password = password;

            user.save();
            return 'Password Changed Successfully';
            
        } catch{
            return 'Authentication Failed';
        }
    }

    async getFile({request , auth}){

        let user;
        try{
              user = await auth.getUser();

        }
        catch(error){
            return 'Authentication failed'; 
        }
            
            // the name (here : uploadedFile ) should be provided in form-data (key-value) pairs 
            const receivedFile = await request.file('uploadedFile' , {
                "types":['image' , 'text'],
                "size" : '10mb'
            });

            await receivedFile.move(Helpers.tmpPath(`uploads/${user.id}`) , {
                name:`data_${user.id}_${receivedFile.clientName}`
            });

            if(!receivedFile.moved()){
                return receivedFile.error();
            }
            return 'File Saved';
 
    }

    async profPic({request , auth}){

        let user;
        try{
            user = await auth.getUser();

        }
        catch(error){
            return 'Authentication failed'; 
        }
        
            // the name (here : uploadedFile ) should be provided in form-data (key-value) pairs 
            const receivedFile = await request.file('pf' , {
                "types":['image'],
                "size" : '10mb'
            });

            await receivedFile.move(Helpers.tmpPath(`uploads/${user.id}`) , {
                name:`pf_${user.id}.${receivedFile.extname}`
            });

            if(!receivedFile.moved()){
                return receivedFile.error();
            }
            return 'Profile Pic Saved'; 
    }

    // we have assumed that pic is saved in .jpg format (it is simply possible to support more ext's)
    async sendPf({response,params}){
        
        const id = params.id; 
        if(!pf)
            return 'Profile Pic Not Found for the user'
        response.header('Content-Type', 'image/jpg');
        response.attachment(Helpers.tmpPath(`uploads/${id}/pf_${id}.jpg`));
    }

}

module.exports = UserController
