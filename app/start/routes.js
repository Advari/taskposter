'use strict'

const { route, RouteResource } = require('@adonisjs/framework/src/Route/Manager');

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// TaskController.home is as same as the method defined in TaskController 
Route
    .get('/', 'TaskController.home');




Route
    .post('/login' , 'UserController.login').validator('LoginUser')

//Chaining a validator to our handler forces to validate the form before creating it. 
Route
    .post('/signup','UserController.create').validator('CreateUser')

// this operation does not need controller 
Route
    .get('/logout' , async({auth , response})=>{
    await auth.logout();
    return response.redirect('/');
})
Route
  .post('/users/:id', 'UserController.update').validator('UpdateUser')
  
Route
  .delete('/users', 'UserController.delete')

Route
  .post('/forget', 'UserController.forget');

Route
  .post('/resetPass/:token', 'UserController.resetPass')

  

// Just for Testing Purposes
// if id is passed, show the specified user, else , show all users ...  
Route
    .get('/users/' , 'UserController.show')




// Upload Files
// Authentication is Done in Controller : Better Approach : User Middleware.
Route
  .post('/upload', 'UserController.getFile')

// Profile Pic 
Route
  .post('/upload/profile-pic', 'UserController.profPic')

Route
  .get('/users/profile-pic/:id','UserController.sendPf')





  
  // Task Handlers 
  Route.group( () => { 
    Route.get('/delete/:id' , 'TaskController.delete'); 
    Route.get('/' , 'TaskController.userIndex')
    Route.get('/task-pic/:id' ,'TaskController.sendTf');
    Route.post('/' , 'TaskController.create').validator('CreateTask')
    Route.post('/update/:id' , 'TaskController.update').validator('CreateTask');
    Route.post('/upload-pic/:id' , 'TaskController.setPic');
}).prefix('/post-a-task').middleware('auth')

